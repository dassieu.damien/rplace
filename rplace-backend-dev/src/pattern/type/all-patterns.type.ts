import { Pattern } from './pattern.type';

export type AllPatterns = {
    'bind': Pattern[],
    'self': Pattern[]
}