export type Pattern  = {
    patternId: string;
    name: string;
    userId: string;
}