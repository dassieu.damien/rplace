export enum EventType {
    INCREASE_MAP = 'game:map',
    UPDATE_TIMER = 'game:timer',
    UPDATE_COLORS = 'game:colors'
}