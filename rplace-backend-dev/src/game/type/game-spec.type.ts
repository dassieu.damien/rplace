import { Color } from "./color.type";

export type GameSpec = {
    timer: number;
    width: number;
    colors: Color[];
}