export type UserSpec = {
    pixelsPlaced: number;
    isGold: boolean;
    bombs: number;
    stickedPixels: number;
    rank: number;
    favColor: string;
    pscope: string;
    username: string;
    steps: {};
    group: string;
}