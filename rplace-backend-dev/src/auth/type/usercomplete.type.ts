export type UserComplete = {
    username: string;
    pscope: string;
    password: string;
}