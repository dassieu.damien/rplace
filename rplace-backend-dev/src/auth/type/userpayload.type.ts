export type UserPayload = {
    username: string;
    pscope: string;
}